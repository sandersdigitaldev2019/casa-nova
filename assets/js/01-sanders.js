//Home
function button_back() {

    $('.mobile-nav > ul > div > .back-button').click(function () {
        $('.mobile-nav ul, .mobile-nav > ul > div, .mobile-nav > ul > li  ').removeClass('actived');
    });
    $('.todos_departamentos-display_scroll .back-button').click(function () {
        $('.todos_departamentos-display_scroll, .todos_departamentos-display_scroll ul, .todos_departamentos-display_scroll li, .todos_departamentos-display_scroll div').removeClass('actived');
        $('.todos_departamentos-display').removeClass('opened');
    });

}

function menu() {
    $('._header2-menu_hamburger img').click(function () {
        $('.mobile-nav').addClass('actived');
        $('.blur').addClass('actived');
    });
    $('.menu-icon, .blur').click(function () {
        $('.mobile-nav').removeClass('actived');
        $('.blur').removeClass('actived');
    });
    $('.mobile-nav > ul > li, .todos_departamentos-display_scroll ul li').click(function () {
        var $selected = $(this).data('id');
        $(this).parent().addClass('actived');
        $(this).addClass('actived');
        $('.'+$selected).addClass('actived');
    });
    $('.todos_departamentos-display_scroll ul li').click(function () {
        $(this).parent().parent().addClass('actived');
        $(this).parent().parent().parent().addClass('opened');
    })

}

$(document).ready(function () {
  menu();
  button_back()
  $('.infoBar-wrapper').slick({
      dots: true,
      autoplay: true,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 420,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
          }
        }
      ]
  });
  $('.slider_product_home').slick({
      dots: false,
      infinite: false,
      speed: 200,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
          {
            breakpoint: 420,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              dots: false
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              dots: false
            }
          }
        ]
  });
});

//Departamento
$(document).ready(function () {
  $(".breadcrumb").css('display', 'none');
  setTimeout(() => {
    $(".breadcrumb").prependTo(".new_breadcrumbs");
    $(".breadcrumb").css('display', 'initial');
  }, 200);
  if ($(window).width() < 768) {
    $(".page-category.departamentos").css('display', 'none');
    setTimeout(() => {
      $(".name-page ").prependTo(".page-category.departamentos");
      $(".page-category.departamentos .name-page + div").prependTo(".page-category.departamentos");
      $(".page-category.departamentos").css('display', 'initial');
    }, 200);
    if ($('.filterColumn').hasClass('ativo')) {
      $('.checkbox, .pricefilter').click(function () { 
        $('.filterColumn').removeClass('ativo');
      });
    }
  }
});

$(document).ready(function() {
  $(".searchcolumn .search .prompt").keyup(function(event) {
    var value = $( this ).val();
    var link = "https://www.lojacasanova.com.br/busca?n="+ value ;
    if(event.keyCode == 13) {
      window.location.href = link ;
      event.preventDefault();
      return false;
    }
  }).keyup();
});


//Formulario
$(document).ready(function () {
  $('#btn_news').click(function (e) { 
    var CheckTextPopUp = '';
      setTimeout(() => {
        setInterval(() => {
          CheckTextPopUp = $('.swal2-content').text();
          if(CheckTextPopUp == 'Cadastro realizado com sucesso!'){
            var new_text = $('.swal2-content').text('Cadastro realizado com sucesso! \n Você receberá o cupom de desconto por e-mail');
            new_text.html(new_text.html().replace(/\n/g,'<br/>'));
          }
        }, 100);
      }, 2000);
  });
});