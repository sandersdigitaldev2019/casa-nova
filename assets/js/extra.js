$(document).ready(function() {
    let screen = $(window).width()
    
    let codigos = {
        
        menuMobile: function () {
            if (screen < 768) {
                $('.menuheader .menu-all .wsmenu-list-all > li > .item').on('click', function(){
                    $(".menuheader .menu-all .wsmenu-list-all > li .item").addClass('gone');
                    $(".menuheader .menu-all .wsmenu-list-all > li").addClass('p-0');
                    $(this).siblings('.megamenu-child.clearfix').removeClass('gone');
                });
                
                $('.back_menu').on('click', function(){
                    $(this).parents('.megamenu-child.clearfix').addClass('gone');
                    $(".menuheader .menu-all .wsmenu-list-all a.item").removeClass('gone');
                    $(".menuheader .menu-all .wsmenu-list-all li").removeClass('p-0');
                });
            }
        },
        
        init: function () {
            codigos.menuMobile()
        }
    }
    codigos.init()
});