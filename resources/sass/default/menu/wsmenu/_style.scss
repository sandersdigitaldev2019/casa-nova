@import "colors";

.wsmenu {
    float: left;
    min-height: $menuHeight;
    margin: 0 auto;
    padding: 0;
    position: relative;
    width: 100%;

    * {
        box-sizing: border-box;
        text-decoration: none;
    }
}

.wsmain {
    margin: 0 auto;
    padding: 0;
    width: 100%;

    html,
    body,
    iframe,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        border: 0;
        margin: 0;
        padding: 0;
        vertical-align: baseline;
    }

    .cl {
        clear: both;
    }

    img {
        border: 0 none;
        max-width: 100%;
    }

    &:before {
        content: "";
        display: table;
    }

    &:after {
        clear: both;
        content: "";
        display: table;
    }

    a:focus {
        outline: none !important;
    }
}

.wsmobileheader {
    display: none;
}

.overlapblackbg {
    display: none;
}

.topmenusearch {
    float: right;
    height: 42px;
    margin: 9px 0 0;
    position: relative;
    width: 100%;

    .searchicon {
        transition: all .7s ease 0s;
    }

    input {
        background-color: $colorgalleryapprox;
        border: 0;
        bottom: 0;
        color: $colorconcordapprox;
        float: right;
        height: 42px;
        margin: 0;
        outline: none;
        padding: 0;
        position: relative;
        right: 0;
        text-indent: 15px;
        top: 0;
        transition: all .7s ease 0s;
        width: 100%;
        z-index: 2;

        &:focus {
            color: $black;

            ~ {
                .btnstyle {
                    background-color: $colorcapecodapprox;
                    color: $white;
                    opacity: .9;

                    .fa {
                        color: $white;
                    }
                }

                .searchicon {
                    color: $white;
                    opacity: 1;
                    z-index: 3;
                }
            }
        }
    }

    .btnstyle {
        background-color: $colorgalleryapprox;
        border: solid 0;
        bottom: 0;
        color: $white;
        cursor: pointer;
        line-height: 30px;
        position: absolute;
        right: 0;
        top: 0;
        transition: all .7s ease 0s;
        width: 42px;
        z-index: 1000;

        .fa {
            color: $colorstardustapprox;
            line-height: 38px;
            margin: 0;
            padding: 0;
            text-align: center;
        }

        &:hover {
            background-color: $white;
        }
    }
}

.wsmenu-list {
    display: table;
    margin: 0 auto;
    padding: 0;
    text-align: left;
    width: 100%;

    > li {
        position:relative;
        display: block;
        float: left;
        margin: 0;
        padding: 0;
        text-align: center;
        width:9%;
        &:nth-child(2),
        &:nth-child(7),
        &:nth-child(8),
        &:nth-child(9),
        &:nth-child(10) {
            width:11%;
        }
        
        &.wssearchbar {
            float: left;
            padding-left: 15px;
            width: 41%;
            display: none;
        }

        &:nth-child(n+11){
            display:none !important;
        }
        i {
            font-weight:800;
        }
        i.dropdown.icon {
            display:none !important;
        }
    }

    > {
        .wscarticon > a .fa.fa-shopping-basket {
            color: $colorconcordapprox;
            display: inline-block;
            font-size: 16px;
            line-height: inherit;
            margin-right: 5px;
        }

        .wsshopmenu > a .fa {
            &.fa-angle-down {
                color: $colorconcordapprox;
                display: inline-block;
                font-size: 13px;
                line-height: inherit;
                margin-left: 11px;
            }

            &.fa-shopping-basket {
                color: $colorconcordapprox;
                display: inline-block;
                font-size: 13px;
                line-height: inherit;
                margin-right: 8px;
            }
        }

        .wsshopmyaccount > a .fa {
            &.fa-angle-down {
                color: $colorconcordapprox;
                display: inline-block;
                font-size: 13px;
                line-height: inherit;
                margin-left: 8px;
            }

            &.fa-align-justify {
                color: $colorconcordapprox;
                display: inline-block;
                font-size: 16px;
                line-height: inherit;
                margin-right: 11px;
            }
        }
    }

    li {
        &:hover {
            > {
                .wsmenu-submenu {
                    opacity: 1;
                    transform: translateY(0);
                    visibility: visible;
                }

                .megamenu {
                    opacity: 1;
                    visibility: visible;
                }
            }

            .wsmenu-submenu {
                display: block;
            }

            .megamenu {
                opacity: 1;
            }
        }

        > {
            .wsmenu-submenu {
                transform: translateY(35px);
                transition: all .3s ease;
                visibility: hidden;
            }

            .megamenu {
                transition: all .3s ease;
                visibility: hidden;
            }
        }
    }

    .wsmenu-submenu {
        .wsmenu-submenu-sub {
            left: 100%;
            margin: 0;
            min-width: 220px;
            opacity: 0;
            padding: 0;
            position: absolute;
            top: 0;

            .wsmenu-submenu-sub-sub {
                left: 100%;
                margin: 0;
                min-width: 220px;
                opacity: 0;
                padding: 0;
                position: absolute;
                top: 0;
            }

            li:hover .wsmenu-submenu-sub-sub {
                background-color: $white;
                border: solid 1px $colorgalleryapprox;
                list-style: none;
                opacity: 1;
                padding: 0;
            }
        }

        li:hover .wsmenu-submenu-sub {
            background-color: $white;
            border: solid 1px $colorgalleryapprox;
            display: block;
            list-style: none;
            opacity: 1;
            padding: 0;
        }
    }
}

.megamenu {
    font-size: 15px;
    left: 0;
    margin: 0;
    opacity: 0;
    position: absolute;
    text-align: left;
    //top: 60px;
    width: 500px;
    z-index: 1200;
    >.megamenu-wrapper {
        overflow:hidden;
        display:block;
        background-color: #212121;
        color: $white;
        padding: 0;
        margin:0;
        list-style:none;
        border:5px solid $white;
    }
    &:after {
        content:"";
        position:absolute;
        top:0;
        left:0;
        display:block;
        width:100%;
        height:100%;
        box-shadow: 0 2500px 0 2500px rgba(0, 0, 0, 0.8);
        z-index:-1;
    }
    .menu-list {
        display:block;
        float:left;
        padding:15px 0 15px 15px;
    }
    .menu-image {
        position:relative;
        display:block;
        float:right;
        &:before {
            content:"";
            position:absolute;
            top:0;
            left:0;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 200px 70px 0 0;
            border-color: #212121 transparent transparent transparent;
            z-index:1;
        }
    }
    li:nth-child(n+7) {
        display:none;
    }
    li.last-item {
        display:block !important;
        a {
            text-decoration:underline;
            font-weight:600;
        }
    }
    li>a {
        display:block;
        color: $white;
        font-size:15px;
        padding:2px 5px;
        /*font-weight:600;
        font-style:italic;
        */
        &:hover {
            color:#f1f100;
            background:#212121 !important;
        }
    }
}

.mobiletext {
    display: none;
}

.wsmenu-submenu {
    background-color: $white;
    border: solid 1px $colorgalleryapprox;
    margin: 0;
    min-width: 176px;
    opacity: 0;
    padding: 0;
    position: absolute;
    top: 58px;
    z-index: 1000;

    li {
        display: block;
        margin: 0;
        padding: 0;
        position: relative;

        a {
            background: $white !important;
            background-image: none !important;
            border-bottom: 1px solid $colorwildsandapprox;
            border-right: 0 solid;
            color: $colorstormdustapprox !important;
            display: block;
            font-size: 12px;
            letter-spacing: normal;
            line-height: 22px;
            padding: 8px 12px;
            text-align: left;
            text-transform: none;
        }

        > .wsmenu-submenu-sub {
            transform: translateY(35px);
            transition: all .3s ease;
            visibility: hidden;
        }

        &:hover > .wsmenu-submenu-sub {
            opacity: 1;
            transform: translateY(0);
            visibility: visible;
        }
    }

    .fa {
        font-size: 15px;
        margin-right: 11px;
    }
}

.wsmenuexpandermain {
    display: none;
}

.wsmenu-click, .wsmenu-click02 {
    display: none;
}

@import "mq";
